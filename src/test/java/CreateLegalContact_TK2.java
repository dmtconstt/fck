import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.AuthorizationPagePage;
import pages.CreateCompanyPagePage;
import pages.MainPagePage;

@DisplayName("ТК_А2 - Создание юридического лица")
public class CreateLegalContact_TK2 extends TestBase {

    @Test
    @DisplayName("ТК_А2 - Создание юридического лица")
    public void createLegalContact() {
        //arrange
        String innCompany = "0106008761";
        String phoneCompany = "9999999999";

        //Открыть тестовый стенд
        MainPagePage mainPagePage = new MainPagePage(this.driver, this.wait);
        mainPagePage.open();
        // авторизация
        AuthorizationPagePage authorizationPage = new AuthorizationPagePage(this.driver, this.wait);
        authorizationPage.authorize();

        //act
        // На основной закладке домашней страницы выбрать закладку модулей «Все».
        mainPagePage.navigationPanelAllPage.allMenu();
        // Из выпадающего списка выбрать пункт «Предприятия» («Accounts»).
        mainPagePage.navigationPanelAllPage.clickAccounts();
        // Раздел создать предприятие
        CreateCompanyPagePage createCompanyPage = new CreateCompanyPagePage(this.driver, this.wait);
        // В меню модуля выбрать пункт «Создать предприятие».
        createCompanyPage.clickCreateCompany();
        // Заполнить поле «ИНН» - 0106008761
        createCompanyPage.setInnCompany(innCompany);
        // Нажать на кнопку «Получить данные с ВП»
        createCompanyPage.getDataFromVP();
        // В разделе «Контактная информация» заполнить поле «Телефон» по маске ХХХХХХХХХХ.
        createCompanyPage.setPhoneСompany(phoneCompany);
        // Ожидание, когда у input появится текст 10701001
        createCompanyPage.waitTextToBePresentInElementValue();
        // Нажать на кнопку «Сохранить и выйти»
        createCompanyPage.clickSaveAndExitButton();
        // Нажать на кнопку «Отменить».
//        createCompany.clickCancel();
    }
}
