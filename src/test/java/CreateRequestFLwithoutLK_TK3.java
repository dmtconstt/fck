import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.*;

import java.util.stream.Collectors;

public class CreateRequestFLwithoutLK_TK3 extends TestBase {

    @Test
    @DisplayName("ТК_А3 - Создание обращения от ФЛ без ЛК")
    public void createRequestFromLKwithoutLK() throws InterruptedException {
        //arrange
        String lastNameRandom = "прпФамилия";
        String firstNameRandom = "арпоИмя";
        String secondNameRandom = "прОтчество";
        String categoryOfContactForDrop = "employee";
        String phone = "9999999999";
        String email = "xxx@xxx.xx";
        String fckEmployee = "fck";
        String consultValue = "consult";
        String subjectValue = "consult_participation";
        String subSubjectValue  = "consult_participation_dates";
        String text = "Текст описания";
        String emailValue = "email";
        //Открыть тестовый стенд
        MainPagePage mainPagePage = new MainPagePage(this.driver, this.wait);
        mainPagePage.open();
        // авторизация
        AuthorizationPagePage authorizationPage = new AuthorizationPagePage(this.driver, this.wait);
        authorizationPage.authorize();

        //act
        // Клик по иконке с аккаунтом в правом верхнем углу
        mainPagePage.navigationPanelAllPage.clickToAccountIconOnTheRight();
        // Открыть модуль «Администрирование».
        mainPagePage.navigationPanelAllPage.clickToAdmin();
        // Раздел Администрирование
        AdministrationPagePage administrationPage = new AdministrationPagePage(this.driver, this.wait);
        // В разделе меню «Система» выбрать «Настройка конфигурации».
        administrationPage.clickConfigSetting();
        // Внизу формы просмотра кликнуть на «Просмотр журнала».
        administrationPage.clickWatchLogs();
        // сохраняем текущее окно
        var initialWindow = driver.getWindowHandle();
        // сохраняем все окна
        var allWindows = driver.getWindowHandles();
        // определяем открытое окно
        var otherWindows = allWindows.stream().filter(x -> !x.equals(initialWindow)).collect(Collectors.toList());
        //переключение на 2 окно
        driver.switchTo().window(otherWindows.stream().findFirst().get());
        // Нажать на кнопку «Установить контрольную точку».
        administrationPage.clickSetControlPoint();
        //переключение на 1 окно
        driver.switchTo().window(initialWindow);
        // В другой вкладке браузера открыть тестовый стенд http://fckproject.itfbgroup.ru/fcktest_001/ и выбрать закладку модулей «Все».
        administrationPage.navigationPanelAllPage.allMenu();
        // В выпадающем списке выбрать пункт «Физические лица» («Contacts»).
        administrationPage.navigationPanelAllPage.clickPhisics();
        // Раздел создать физ лицо
        CreateContactPagePage createContactPage = new CreateContactPagePage(this.driver,this.wait);
        // В меню модуля выбрать пункт «Создать новое физическое лицо».
        createContactPage.clickMakeContact();
        // В разделе «Основная информация» заполнить поле «Фамилия» - рандомный набор букв на кириллице от 3 до 12 символов.
        createContactPage.setLastNameInput(lastNameRandom);
        // В разделе «Основная информация» заполнить поле «Имя» - рандомный набор букв на кириллице от 3 до 12 символов.
        createContactPage.setFirstNameInput(firstNameRandom);
        // В разделе «Основная информация» заполнить поле «Отчество» - рандомный набор букв на кириллице от 3 до 12 символов.
        createContactPage.setSecondNameInput(secondNameRandom);
        // В разделе «Основная информация» заполнить поле «Категория контакта» - нажать на поле и выбрать из выпадающего списка значение «Сотрудник предприятия»
        createContactPage.setCategoryOfContactDrop(categoryOfContactForDrop);
        // В разделе «Основная информация» заполнить поле «Предприятие» - нажать на кнопку со стрелкой справа от поля.
        createContactPage.clickButtonCompany();
        // получить все окна
        var getAll3windows = driver.getWindowHandles();
        // получить 3 окно по индексу
        var getTo3window = getAll3windows.toArray()[2];
        //переключение на 3 окно (Поиск предприятий)
        driver.switchTo().window(String.valueOf(getTo3window));
        //окно с поиском предприятий
        EnterpriseSearchWindowPagePage enterpriseSearchWindowPage = new EnterpriseSearchWindowPagePage(this.driver,this.wait);
        //Заполнить поле «ИНН» - 6321277661
        enterpriseSearchWindowPage.setInnAdvanced();
        //Нажать на кнопку «Найти».
        enterpriseSearchWindowPage.clickSearchButton();
        // Выбрать найденное предприятие.
        enterpriseSearchWindowPage.clickFindedCompany();
        //Переходим в раздел "Создать физ лицо".
        driver.switchTo().window(initialWindow);
        // В форме «Основная информация» заполнить поле «Должность» - менеджер.
        createContactPage.setPosition();
        // В разделе «Контактные данные» заполнить по маске ХХХХХХХХХХ поле «Телефон»
        createContactPage.setPhone(phone);
        // В подразделе «Контактные данные» заполнить поле «E-mail» по маске  XXX@XXX.XX.
        createContactPage.setEmail(email);
        // Под Формой создания физического лица нажать на кнопку «Сохранить и выйти».
        createContactPage.clickSaveExit();
        // На основной закладке выбрать закладку модулей «Все».
        createContactPage.navigationPanelAllPage.allMenu();
        // Из выпадающего списка выбрать пункт «Обращения».
        createContactPage.navigationPanelAllPage.clickToAppealsMenu();
        // Переход в меню «Создать обращение».
        CreateAppealPagePage createAppealPage = new CreateAppealPagePage(this.driver,this.wait);
        // В меню модуля выбрать пункт «Создать обращение».
        createAppealPage.clickCreateAppeal();
        // В разделе «Контактная информация» заполнить поле «Контактное лицо» - нажать на стрелку справа от поля.
        createAppealPage.clickArrowOnTheRightOfInputContactFace();
        // получить все окна
        var getAllWindows = driver.getWindowHandles();
        // получить 3 окно по индексу
        var getToNewWindow = getAllWindows.toArray()[2];
        //переключение на 3 окно (Поиск предприятий)
        driver.switchTo().window(String.valueOf(getToNewWindow));
        // В поле «ФИО» ввести %<Фамилия>%, где <Фамилия>=Фамилия, созданного физического лица.
        createAppealPage.setFindingName(lastNameRandom);
        // Нажать на кнопку «Найти»
        createAppealPage.clickSearchButton();
        // Выбрать физическое лицо, созданное ранее, из формы просмотра списка физических лиц.
        createAppealPage.clickFirstFindName();
        // В разделе «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»
        createAppealPage.clickCategorySelector();
        // переключение на 1 вкладку
        driver.switchTo().window(String.valueOf(initialWindow));
        // Выбрать из выпадающего списка значение «Сотрудник ФЦК»
        createAppealPage.setCategoryByValue(fckEmployee);
        // В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».
        createAppealPage.clickSubtypeSelector();
        // Выбрать из выпадающего списка значение «Консультация».
        createAppealPage.setConsultValue(consultValue);
        // В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».
        createAppealPage.clickSubjectSelector();
        // Выбрать любое значение из выпадающего списка.
        createAppealPage.setSubjectValue(subjectValue);
        // В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».
        createAppealPage.clickSubSubjectSelector();
        // Выбрать любое значение из выпадающего списка.
        createAppealPage.setSubSubjectValue(subSubjectValue);
        // переключение на фрэйм3 с полем для ввода
        createAppealPage.switchFrame();
        // В поле «Описание» вручную ввести текст – «Текст описания»
        createAppealPage.setTinymceValue(text);
        // переключение на Default iFrame
        createAppealPage.switchToDefaultFrame();
        // В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».
        createAppealPage.clickConnect();
        // Из выпадающего списка выбрать «Email».
        createAppealPage.setConnectValue(emailValue);
        // В поле «Email для связи» выбрать из выпадающего списка сохраненный Email.
        createAppealPage.setEmailForConnect();
        // Нажать на кнопку «Сохранить и выйти».
        createAppealPage.clickSaveAndExit();
    }
}
