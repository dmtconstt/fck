import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.AuthorizationPagePage;
import pages.CreateAppealPagePage;
import pages.MainPagePage;

public class CreatingRequestFromAnonymousUser_TK5 extends TestBase {
    @Test
    @DisplayName("ТК_А5 - Создание обращения от анонимного пользователя")
    public void creatingRequestFromAnonymousUser() {
        //arrange
        String value = "fck";
        String consultValue = "consult";
        String consultFckValue = "consult_fck";
        String consultFckOtherValue = "consult_fck_other";
        String text = "Текст описания";
        String phoneValue = "phone";
        String phoneAnonValue = "9998887766";

        //Открыть тестовый стенд
        MainPagePage mainPagePage = new MainPagePage(this.driver, this.wait);
        mainPagePage.open();
        // авторизация
        AuthorizationPagePage authorizationPage = new AuthorizationPagePage(this.driver, this.wait);
        authorizationPage.authorize();
        // На основной закладке выбрать закладку модулей «Все».
        mainPagePage.navigationPanelAllPage.allMenu();
        // Из выпадающего списка выбрать пункт «Обращения».
        mainPagePage.navigationPanelAllPage.clickToAppealsMenu();
        // Переход в меню «Создать обращение».
        CreateAppealPagePage createAppealPage = new CreateAppealPagePage(this.driver,this.wait);
        // В меню модуля выбрать пункт «Создать обращение».
        createAppealPage.clickCreateAppeal();
        // В разделе «Контактная информация» поставить галочку в чек-бокс «Анонимно?»
        createAppealPage.clickAnonCheck();
        // В подменю «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»
        createAppealPage.clickCategory();
        // Выбрать из выпадающего списка значение «Сотрудник ФЦК»
        createAppealPage.setSelectEmployeeFck(value);
        // В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».
        createAppealPage.clickSubtype();
        // Выбрать из выпадающего списка значение «Консультация».
        createAppealPage.setSelectSubtypeInvitation(consultValue);
        // В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».
        createAppealPage.clickSubject();
        // Выбрать из выпадающего списка значение «Работа ФЦК».
        createAppealPage.setSelectSubjectEvents(consultFckValue);
        // В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».
        createAppealPage.clickSubSubject();
        // Выбрать из выпадающего списка выбрать значение «Другое».
        createAppealPage.setSelectSubSubject(consultFckOtherValue);
        // Переключение на фрейм 3
        createAppealPage.switchFrame();
        // В поле «Описание» вручную ввести текст – «Текст описания
        createAppealPage.setTinymceValue(text);
        // переключение на Default iFrame
        createAppealPage.switchToDefaultFrame();
        // В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».
        createAppealPage.clickConnectAnon();
        // Из выпадающего списка выбрать «Телефон».
        createAppealPage.setConnectAnonValue(phoneValue);
        // В поле «Телефон для связи» указать телефон по маске XXXХХХXXXX.
        createAppealPage.setConnectAnonPhone(phoneAnonValue);
        // Нажать на кнопку «Сохранить и выйти».
        createAppealPage.clickSaveAndExit();

        // Assert
        Assertions.assertTrue(createAppealPage.getConnectAnonPhone().getText().contains(phoneAnonValue),"В поле «Контактный телефон» != phoneAnonValue");

    }
}
