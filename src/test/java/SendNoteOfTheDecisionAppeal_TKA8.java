
import com.microsoft.playwright.*;
import com.microsoft.playwright.options.SelectOption;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import pages.AuthorizationPagePage;

import java.util.List;

public class SendNoteOfTheDecisionAppeal_TKA8 extends TestBase{
    /**
     * Тест на Playwright
     */
    private String name = "d.tmenov";
    private String pass = "123Pass";
     String url = "http://fckproject.itfbgroup.ru/auto/";

    @Test
    @DisplayName("ТК_А8 – Отправка уведомления о решении по обращению")
    public void sendNoteOfTheAppeal() throws InterruptedException {
        //arrange
        // подключаюсь к Playwright
        // Открыть тестовый стенд http://fckproject.itfbgroup.ru/fcktest_001/.
        Playwright playwright = Playwright.create();
        Browser browser = playwright.chromium().launch(new BrowserType.LaunchOptions().setHeadless(false));
        BrowserContext context = browser.newContext();
        Page page = context.newPage();
        page.navigate(url);
        // Авторизация
        page.fill("#user_name", name);
        page.fill("#username_password", pass);
        page.locator("id=bigbutton").click();
        // Открыть модуль «Администрирование».
        page.locator("(//*[contains(@id,'with-label')])[1]").click();
        page.locator("(//*[contains(@id,'admin_link')])[3]").click();
        // В разделе меню «Система» выбрать «Настройка конфигурации».
        page.locator("#configphp_settings").click();
        // Внизу формы просмотра кликнуть на «Просмотр журнала».
        // переключение на 2 новое окно
        Page page2 = context.waitForPage(
                () -> page.locator("(//*[@target='_blank'])[1]").click()
        );
        // Нажать на кнопку «Установить контрольную точку».
        page2.locator("//*[@name='mark']").click();
        // В другой вкладке браузера открыть тестовый стенд http://fckproject.itfbgroup.ru/auto/
        // переключение на 3 новое окно
        Page page3 = context.newPage();
        page3.navigate("http://fckproject.itfbgroup.ru/auto/");
        // выбрать закладку модулей «Все».
        page3.locator("#grouptab_1").hover();
        // Из выпадающего списка выбрать пункт «Обращения».
        page3.locator("(//*[contains(text(),\"Обращения\")])[3]").click();
        // В меню модуля выбрать пункт «Создать обращение».
        page3.locator("(//*[contains(@class,'actionmenulink')])[1]").click();
        // Перейти на новое окно
        // В разделе «Контактная информация» заполнить поле «Контактное лицо» - нажать на стрелку справа от поля.
        Page window2 = context.waitForPage(
                () -> page3.locator("//*[contains(@id,\"btn_contact_created_by_name\")]").click()
        );
        // В поле «ФИО» ввести Фамилию физического лица из предусловия.
        window2.fill("#full_name_advanced","Ласточкин Борис Васильевич");
        // Нажать на кнопку «Найти»
        window2.locator("#search_form_submit").click();
        // Выбрать физическое лицо из формы просмотра списка физических лиц.
        window2.locator("(//*[contains(@class,'oddListRowS1')] /a)[1]").click();
        // В разделе «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»
        page3.locator("#category").click();
        // Выбрать из выпадающего списка значение «Сотрудник ФЦК»
        page3.locator("#category").selectOption("Сотрудник ФЦК");
        // В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».
        page3.locator("#subtype").click();
        // Выбрать из выпадающего списка значение «Консультация».
        page3.locator("#subtype").selectOption("Консультация");
        // В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».
        page3.locator("#subject").click();
        // Выбрать из выпадающего списка значение «Работа платформы».
        page3.locator("#subject").selectOption("Работа платформы");
        // В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».
        page3.locator("#subsubject").click();
        // Выбрать из выпадающего списка значение «Регистрация на портале».
        page3.locator("#subsubject").selectOption("Регистрация на портале");
        // переключение на iFrame
        // В поле «Описание» вручную ввести текст – «Текст описания»
        page3.frameLocator("(//*/iframe)[3]").locator("#tinymce").fill("Текст описания");
        // В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».
        page3.locator("#connect").click();
        // Из выпадающего списка выбрать «Email».
        page3.locator("#connect").selectOption("Email");
        // В поле «Email для связи» указать Email из предусловия.
        page3.selectOption("#contact_emails",new SelectOption().setIndex(1));
        // Нажать на кнопку «Сохранить и выйти».
        page3.locator("(//*[contains(@class,\"button primary\")])[2]").click();
        // В форме просмотра нажать на кнопку «Взять в работу».
        page3.getByText("Взять в работу").click();
        // В разделе «Решение» в поле «Решение» ввести решение по обращению - «Текст решения».
        page3.locator("#resolution").fill("Текст решения");
        // Нажать на кнопку «Сохранить и выйти».
        page3.locator("(//*[contains(@id,'SAVE')])[2]").click();
        // Открыть модуль «Администрирование».
        page.locator("(//*[contains(@id,'with-label')])[1]").click();
        page.locator("(//*[contains(@id,'admin_link')])[3]").click();
        // Открыть вкладку браузера с логом системы.
        page.locator("#CustomRestLogView").click();
        // Нажать на кнопку «Показать с контрольной точки».
        page.getByText("Показать с контрольной точки").click();

//        Assertions.assertEquals(page.locator("custom_log_out"));


    }
}
