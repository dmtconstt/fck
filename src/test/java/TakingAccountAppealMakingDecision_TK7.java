//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import pages.AuthorizationPagePage;
//import pages.CreateAppealPagePage;
//import pages.MainPagePage;
//
//import static com.codeborne.selenide.Selenide.*;
//
//public class TakingAccountAppealMakingDecision_TK7 extends TestBase {
//    /**
//     * Тест на Selenide
//     */
//    protected String url = "http://fckproject.itfbgroup.ru/auto/";
//
//    @Test
//    @DisplayName("ТК_А7 - Взятие в работу обращения и вынесение решения")
//    public void accountAppealMakingDecision() throws InterruptedException {
//        //arrange
//        String fio = "Ласточкин Борис Васильевич";
//        String value = "fck";
//        String consultValue = "consult";
//        String workPlatform = "consult_tech";
//        String consultRegistrationValue = "consult_tech_registration";
//        String text = "Текст описания";
//        String emailValue = "email";
//        // Открыть тестовый стенд http://fckproject.itfbgroup.ru/fcktest_001/.
//        MainPagePage mainPagePage = new MainPagePage();
//        open(url);
//        // Авторизация
//        new AuthorizationPagePage();
//
//        //act
//        // Выбрать закладку модулей «Все».
//        mainPagePage.navigationPanelAllPage.allMenuS();
//        // Из выпадающего списка выбрать пункт «Обращения».
//        mainPagePage.navigationPanelAllPage.clickToAppealsMenuS();
//        // Переход в меню «Создать обращение».
//        CreateAppealPagePage createAppealPage = new CreateAppealPagePage();
//        // В меню модуля выбрать пункт «Создать обращение».
//        createAppealPage.clickCreateAppealS();
//        // В разделе «Контактная информация» заполнить поле «Контактное лицо» - нажать на стрелку справа от поля.
//        createAppealPage.clickToArrowContactS();
//        // переключаемся на другое окно
//        switchTo().window(1);
//        // В поле «ФИО» ввести Фамилию физического лица из предусловия.
//        createAppealPage.setSearchFioS(fio);
//        // Нажать на кнопку «Найти»
//        createAppealPage.clickButtonSearchS();
//        // Выбрать физическое лицо из формы просмотра списка физических лиц.
//        createAppealPage.clickFindedFioS();
//        // переключение на 1 окно
//        switchTo().window(0);
//        // В разделе «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»
//        createAppealPage.clickCategoryS();
//        // Выбрать из выпадающего списка значение «Сотрудник ФЦК»
//        createAppealPage.setSelectEmployeeFckS(value);
//        // В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».
//        createAppealPage.clickSubtypeS();
//        // Выбрать из выпадающего списка значение «Консультация».
//        createAppealPage.setConsultValueS(consultValue);
//        // В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».
//        createAppealPage.clickSubjectSelectorS();
//        // Выбрать из выпадающего списка значение «Работа платформы».
//        createAppealPage.setSelectSubjectEventS(workPlatform);
//        // В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».
//        createAppealPage.clickSubSubjectS();
//        // Выбрать из выпадающего списка значение «Регистрация на портале».
//        createAppealPage.setSelectSubSubjectS(consultRegistrationValue);
//        // Переключение на фрейм 3
//        switchTo().frame(2);
//        // В поле «Описание» вручную ввести текст – «Текст описания
//        createAppealPage.setTinymceValueS(text);
//        // Переключение на фрейм 1
//        switchTo().defaultContent();
//        // В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».
//        createAppealPage.clickConnectS();
//        // Из выпадающего списка выбрать «Email».
//        createAppealPage.setConnectValueS(emailValue);
//        // В поле «Email для связи» указать Email из предусловия.
//        createAppealPage.setEmailForConnectS();
//        // Нажать на кнопку «Сохранить и выйти».
//        createAppealPage.clickSaveAndExitS();
//        // В форме просмотра нажать на кнопку «Взять в работу».
//        createAppealPage.workPlatformPagePage.clickButtonTakeWork();
//        // В разделе «Решение» в поле «Решение» ввести решение по обращению - «Текст решения».
//        createAppealPage.workPlatformPagePage.setTextIntoTextarea(text);
//        // Нажать на кнопку «Сохранить и выйти».
//        createAppealPage.workPlatformPagePage.clickExitAndSave();
//
//        // Asserts
//        // Поле «Состояние» заполнено – «Закрыто».
//        Assertions.assertTrue(createAppealPage.workPlatformPagePage.state.getAttribute("value").equals("Closed"),
//                "Поле «Состояние» не заполнено – «Закрыто». ");
//        // Поле «Статус» заполнено – «Решено».
//        Assertions.assertTrue(createAppealPage.workPlatformPagePage.status.getAttribute("value").equals("Closed_Closed"),
//                "Поле «Статус» не заполнено – «Решено»");
//        // Поле «Ответственный» заполнено – указан текущий пользователь (Автотест)
//        Assertions.assertTrue(createAppealPage.workPlatformPagePage.assignedUser.getText().equals("Тменов Дмитрий"),
//                "Поле «Ответственный» не заполнено – указан текущий пользователь (Автотест)");
//    }
//}
