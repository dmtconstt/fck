package pages;


import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$;

public class AuthorizationPagePage extends MainPagePage {
    private String name = "d.tmenov";
    private String pass = "123Pass";

    //locator
    @FindBy(id = "user_name")
    private WebElement login;

    @FindBy(id = "username_password")
    private WebElement password;

    @FindBy(id = "bigbutton")
    private WebElement button;

    // Selenide
//    public SelenideElement log = $("#user_name");
//    public SelenideElement pas = $("#username_password");
//    public SelenideElement but = $("#bigbutton");


    public AuthorizationPagePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver,this);
    }

    // Selenide
//    public AuthorizationPagePage() {
//        log.sendKeys(name);
//        pas.sendKeys(pass);
//        but.click();
//    }


    @Step("Авторизация")
    public void authorize() {
        this.login.sendKeys(name);
        this.password.sendKeys(pass);
        this.button.click();
        new AuthorizationPagePage(this.driver, this.wait);
    }


}
