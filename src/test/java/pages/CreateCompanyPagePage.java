package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateCompanyPagePage extends MainPagePage {

    @FindBy(xpath = "(//*[.=\"Создать предприятие\"])[6]")
    private WebElement createСompany;

    @FindBy(css = "#inn")
    private WebElement innСompany;

    @FindBy(css = ".phone_number")
    private WebElement phoneСompany;

    @FindBy(xpath = "(//*[contains(@value,'Получить данные с ВП')])[1]")
    private WebElement getDataFromVP;

    @FindBy(xpath = "(//*[contains(@id,'SAVE')])[1]")
    private WebElement saveAndExit;

    @FindBy(css = "#kpp")
    public WebElement kpp;

    @FindBy(xpath = "(//*[contains(@value,'  Отменить  ')])[1]")
    public WebElement cancelButton;

    public CreateCompanyPagePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver,this);
    }

    @Step("В меню модуля выбрать пункт «Создать предприятие».")
    public void clickCreateCompany(){
        createСompany.click();
    }

    @Step("Заполнить поле «ИНН» - {innCompany}")
    public void setInnCompany(String innCompany) {
        innСompany.sendKeys(innCompany);
    }

    @Step("Нажать на кнопку «Получить данные с ВП»")
    public void getDataFromVP() {
        getDataFromVP.click();
    }

    @Step("В разделе «Контактная информация» заполнить поле «Телефон» = {phoneСompan} по маске ХХХХХХХХХХ.")
    public void setPhoneСompany(String phoneСompan) {
        phoneСompany.sendKeys(phoneСompan);
    }

    @Step("Ожидание, когда у input появится текст 10701001")
    public void waitTextToBePresentInElementValue() {
        wait.until(ExpectedConditions.textToBePresentInElementValue(kpp, "10701001"));
    }

    @Step("Нажать на кнопку «Сохранить и выйти»")
    public void clickSaveAndExitButton() {
        new Actions(driver)
                .moveToElement(saveAndExit)
                .click()
                .perform();
    }

    public void clickCancel() {
        cancelButton.click();
    }


}
