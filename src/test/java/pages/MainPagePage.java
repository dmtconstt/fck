package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPagePage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected String url = "http://fckproject.itfbgroup.ru/auto/";

    protected String subUrl = "";

    public NavigationPanelAllPage navigationPanelAllPage;
    public WorkPlatformPagePage workPlatformPagePage;

    // for Selenium
    public MainPagePage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
        navigationPanelAllPage = new NavigationPanelAllPage(driver);
    }

    public MainPagePage() {}

    // for Selenide
//    public MainPagePage() {
//        navigationPanelAllPage = new NavigationPanelAllPage();
//        workPlatformPagePage = new WorkPlatformPagePage();
//    }

    //metods
    public void open() {
        this.driver.navigate().to(this.getPageUrl());
//        this.driver.get(getPageUrl());
    }

    protected String getPageUrl() {
        return this.url + this.subUrl;
    }


}



