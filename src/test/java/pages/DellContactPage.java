package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DellContactPage {
    private WebDriver driver;

    @FindBy(css = "#tab-actions .dropdown-toggle")
    private WebElement dropActionsContact;

    @FindBy(css = "#delete_button")
    private WebElement dellContact;

    public DellContactPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void dellContact(){
        new Actions(driver)
                .moveToElement(dropActionsContact)
                .click()
                .moveToElement(dellContact)
                .click()
                .perform();
    }


}
