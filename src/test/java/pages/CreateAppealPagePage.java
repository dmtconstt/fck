package pages;

//import com.codeborne.selenide.Condition;
//import com.codeborne.selenide.SelenideElement;
//import static com.codeborne.selenide.Selectors.byXpath;
//import static com.codeborne.selenide.Selenide.$;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



public class CreateAppealPagePage extends MainPagePage {

    @FindBy(xpath = "(//*[contains(@class,'actionmenulink')])[1]")
    private WebElement createAppeal;

    // selenide
//    public SelenideElement createAppealS = $(byXpath("(//*[contains(@class,'actionmenulink')])[1]"));

    @FindBy(xpath = "//*[contains(@id,\"btn_contact_created_by_name\")]")
    private WebElement arrowOnTheRightOfInputContactFace;

    @FindBy(css = "#full_name_advanced")
    private WebElement inputFindName;

    @FindBy(css = "#search_form_submit")
    private WebElement searchButton;

    @FindBy(xpath = "(//*[contains(@class,'oddListRowS1')] /a)[1]")
    private WebElement firstFindName;

    @FindBy(css = "#category")
    private WebElement categorySelector;

    @FindBy(css = "#subtype")
    private WebElement subtypeSelector;

    // // selenide
//    public SelenideElement subtypeSelectorS = $("#subtype");

    @FindBy(css = "#subject")
    private WebElement subjectSelector;

    // Selenide
//    public SelenideElement subjectSelectorS = $("#subject");

    @FindBy(css = "#subsubject")
    private WebElement subsubjectSelector;

    @FindBy(css = "#tinymce p")
    private WebElement tinymce;

    // selenide
//    public SelenideElement tinymceS = $("#tinymce p");

    @FindBy(xpath = "(//*/iframe)[3]")
    private WebElement iframe3;

    @FindBy(css = "#connect")
    private WebElement connect;

    @FindBy(css = "#connect_anon_selection")
    private WebElement connectAnon;

    // selenide
//    public SelenideElement connectS = $("#connect");

    @FindBy(css = "#contact_emails")
    private WebElement emailForConnect;

    // selenide
//    public SelenideElement emailForConnectS = $("#contact_emails");

    @FindBy(xpath = "(//*[contains(@class,\"button primary\")])[2]")
    private WebElement saveAndExit;

    // Selenide
//    public SelenideElement saveAndExitS = $(byXpath("(//*[contains(@class,\"button primary\")])[2]"));

    @FindBy(css = "#btn_contact_created_by_name")
    private WebElement arrowContactFace;

    // selenide
//    public SelenideElement arrowContactFaceS = $("#btn_contact_created_by_name");

    @FindBy(css = "#full_name_advanced")
    private WebElement inputFioSearch;

    // selenide
//    public SelenideElement inputFioSearchS = $("#full_name_advanced");

    @FindBy(css = "#email_advanced")
    private WebElement inputEmailSearch;

    @FindBy(css = "#search_form_submit")
    private WebElement buttonSearch;

    // selenide
//    public SelenideElement buttonSearchS = $("#search_form_submit");

    @FindBy(xpath = "(//*[contains(@class, \"oddListRowS1\")] // a)[1]")
    private  WebElement firstFindFio;

    // selenide
//    public SelenideElement firstFindFioS = $(byXpath("(//*[contains(@class, \"oddListRowS1\")] // a)[1]"));

    @FindBy(css = "#category")
    private WebElement category;

    // selenide
//    public SelenideElement categoryS = $("#category");

    @FindBy(css = "#subtype")
    private WebElement subtype;

    // selenide
//    public SelenideElement subtypeS = $("#subtype");

    @FindBy(css = "#subject")
    private WebElement subject;

    // selenide
//    public SelenideElement subjectS = $("#subject");

    @FindBy(css = "#subsubject")
    private WebElement subsubject;

    // selenide
//    public SelenideElement subsubjectS = $("#subsubject");

    @FindBy(css = "#contact_emails")
    private WebElement contact_emails;

    @FindBy(xpath = "//*[contains(@id,\"contact_emails\")]/option[contains(@value, \"test\")]")
    private WebElement contactEmailValueWithTest;

    @FindBy(xpath = "//*[contains(@id,\"tab-content-0\")] /div[2] / div[contains(@class,\"col-xs-12 col-sm-6 detail-view-row-item\")][2]")
    private WebElement dontAssigned;

    @FindBy(xpath = "//*[contains(@id,\"tab-content-0\")] /div[2] / div[contains(@class,\"col-xs-12 col-sm-6 detail-view-row-item\")][1]")
    private WebElement conditionInput;

    @FindBy(xpath = "(//*[contains(@id,'top-panel-0')] // div[contains(@class,'col-xs-12 col-sm-8 detail-view-field ')])[8]")
    private WebElement connectAnonPhoneForAssert;

    @FindBy(css = "#anon")
    private WebElement anon;

    @FindBy(css = "#connect_anon_phone")
    private WebElement connectAnonPhone;

    @FindBy(xpath = "//*[contains(text(),'Назначить ответственного')]")
    private WebElement appointAResponsible;

    public CreateAppealPagePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    // Selenide
//    public CreateAppealPagePage() {}

    @Step("В меню модуля выбрать пункт «Создать обращение».")
    public void clickCreateAppeal() {
        createAppeal.click();
    }

    // selenide
//    @Step("В меню модуля выбрать пункт «Создать обращение».")
//    public void clickCreateAppealS() {
//        createAppealS.shouldBe(Condition.visible).click();
//    }

    @Step("В разделе «Контактная информация» заполнить поле «Контактное лицо» - нажать на стрелку справа от поля.")
    public void clickArrowOnTheRightOfInputContactFace() {
        arrowOnTheRightOfInputContactFace.click();
    }

    @Step("В поле «ФИО» ввести Фамилия физического лица из предусловия = {inputName}.")
    public void setFindingName(String inputName) {
        inputFindName.sendKeys(inputName);
    }

    @Step("Нажать на кнопку «Найти»")
    public void clickSearchButton() {
        searchButton.click();
    }

    @Step("Выбрать физическое лицо, созданное ранее, из формы просмотра списка физических лиц.")
    public void clickFirstFindName() {
        firstFindName.click();
    }

    @Step("В разделе «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»")
    public void clickCategorySelector() {
        new Actions(driver)
                .moveToElement(categorySelector)
                .click();
    }

    @Step("Выбрать из выпадающего списка значение «Сотрудник ФЦК» = {value}")
    public void setCategoryByValue(String value) {
        var category = new Select(categorySelector);
        category.selectByValue(value);
    }

    @Step("В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».")
    public void clickSubtypeSelector() {
        new Actions(driver)
                .moveToElement(subtypeSelector)
                .click()
                .perform();
    }

    @Step("Выбрать из выпадающего списка значение «Консультация» = {value}.")
    public void setConsultValue(String value) {
        var category = new Select(subtypeSelector);
        category.selectByValue(value);
    }
    // Selenide
//    @Step("Выбрать из выпадающего списка значение «Консультация» = {value}.")
//    public void setConsultValueS(String value) {
//        subtypeSelectorS.selectOptionByValue(value);
//    }

    @Step("В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».")
    public void clickSubjectSelector() {
        new Actions(driver)
                .moveToElement(subjectSelector)
                .click()
                .perform();
    }

    // Selenide
//    @Step("В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».")
//    public void clickSubjectSelectorS() {
//        subjectSelectorS.scrollTo().hover();
//    }

    @Step("Выбрать любое значение из выпадающего списка. Например- {value} ")
    public void setSubjectValue(String value) {
        var category = new Select(subjectSelector);
        category.selectByValue(value);
    }

    @Step("В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».")
    public void clickSubSubjectSelector() {
        new Actions(driver)
                .moveToElement(subsubjectSelector)
                .click()
                .perform();
    }

    @Step("Выбрать любое значение из выпадающего списка. например - {value}")
    public void setSubSubjectValue(String value) {
        var category = new Select(subsubjectSelector);
        category.selectByValue(value);
    }

    @Step("В поле «Описание» вручную ввести текст – «Текст описания = {text}")
    public void setTinymceValue(String text) {
        tinymce.sendKeys(text);
    }

    // Selenide
//    @Step("В поле «Описание» вручную ввести текст – «Текст описания = {text}")
//    public void setTinymceValueS(String text) {
//        tinymceS.sendKeys(text);
//    }

    public void switchFrame() {
        driver.switchTo().frame(iframe3);
    }

    @Step("В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».")
    public void clickConnect() {
        new Actions(driver)
                .moveToElement(connect)
                .click();
    }

    @Step("В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».")
    public void clickConnectAnon() {
        new Actions(driver)
                .moveToElement(connectAnon)
                .click();
    }

    // Selenide
//    @Step("В разделе «Желаемый способ связи» - нажать на поле «Желаемый способ связи».")
//    public void clickConnectS() {
//        connectS.click();
//    }

    @Step("Из выпадающего списка выбрать «Email» = {email}.")
    public void setConnectValue(String email) {
        var connec = new Select(connect);
        connec.selectByValue(email);
    }

    // Selenide
//    @Step("Из выпадающего списка выбрать «Email» = {email}.")
//    public void setConnectValueS(String email) {
//        connectS.selectOptionByValue(email);
//    }

    @Step("Из выпадающего списка выбрать «Телефон» = {phone}.")
    public void setConnectAnonValue(String phone) {
        var connecAnon = new Select(connectAnon);
        connecAnon.selectByValue(phone);
    }

    public void switchToDefaultFrame() {
        driver.switchTo().defaultContent();
    }

    @Step("В поле «Email для связи» выбрать из выпадающего списка сохраненный Email.")
    public void setEmailForConnect() {
        new Select(emailForConnect)
                .selectByIndex(1);
    }

    // Selenide
//    @Step("В поле «Email для связи» выбрать из выпадающего списка сохраненный Email.")
//    public void setEmailForConnectS() {
//        emailForConnectS.selectOption(1);
//    }

    @Step("Нажать на кнопку «Сохранить и выйти».")
    public void clickSaveAndExit() {
        new Actions(driver)
                .moveToElement(saveAndExit)
                .click()
                .perform();
    }

    // Selenide
//    @Step("Нажать на кнопку «Сохранить и выйти».")
//    public void clickSaveAndExitS() {
//        saveAndExitS.scrollTo().click();
//    }

    //Selenium menot
    @Step("В разделе «Контактная информация» заполнить поле «Контактное лицо» - нажать на стрелку справа от поля.")
    public void clickToArrowContact() {
        arrowContactFace.click();
    }

    // Selenide metod
//    @Step("В разделе «Контактная информация» заполнить поле «Контактное лицо» - нажать на стрелку справа от поля.")
//    public void clickToArrowContactS() {
//        arrowContactFaceS.click();
//    }

    // Selenium metod
    @Step("В поле «ФИО» ввести фамилию физического лица из предусловия = {fio}.")
    public void setSearchFio(String fio) {
        inputFioSearch.sendKeys(fio);
    }

    // Selenide metod
//    @Step("В поле «ФИО» ввести фамилию физического лица из предусловия = {fio}.")
//    public void setSearchFioS(String fio) {
//        inputFioSearchS.sendKeys(fio);
//    }

    @Step("В поле «Любой E-mail» ввести E-mail физического лица из предусловия = {email}.")
    public void setSearchEmail(String email) {
        inputEmailSearch.sendKeys(email);
    }

    @Step("Нажать на кнопку «Найти»")
    public void clickButtonSearch() {
        buttonSearch.click();
    }
    // Selenide
//    @Step("Нажать на кнопку «Найти»")
//    public void clickButtonSearchS() {
//        buttonSearchS.click();
//    }

    @Step("Выбрать физическое лицо из формы просмотра списка физических лиц.")
    public void clickFindedFio() {
        firstFindFio.click();
    }

    // Selenide
//    @Step("Выбрать физическое лицо из формы просмотра списка физических лиц.")
//    public void clickFindedFioS() {
//        firstFindFioS.click();
//    }

    @Step("В разделе «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»")
    public void clickCategory() {
        category.click();
    }

    // Selenide
//    @Step("В разделе «Основная информация» заполнить поле «Категория» - нажать на поле «Категория»")
//    public void clickCategoryS() {
//        categoryS.click();
//    }

    @Step("Выбрать из выпадающего списка значение «Сотрудник ФЦК» = {value}")
    public void setSelectEmployeeFck(String value) {
        new Select(category).selectByValue(value);
    }

    // Selenide
//    @Step("Выбрать из выпадающего списка значение «Сотрудник ФЦК» = {value}")
//    public void setSelectEmployeeFckS(String value) {
//        categoryS.selectOptionByValue(value);
//    }

    @Step("В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».")
    public void clickSubtype() {
        subtype.click();
    }

    // Selenide
//    @Step("В разделе «Основная информация» заполнить поле «Подтип» - нажать на поле «Подтип».")
//    public void clickSubtypeS() {
//        subtypeS.click();
//    }

    @Step("Выбрать из выпадающего списка значение «Приглашение».")
    public void setSelectSubtypeInvitation(String value) {
        new Select(subtype).selectByValue(value);
    }

    @Step("В разделе «Основная информация» заполнить поле «Тема» - нажать на поле «Тема».")
    public void clickSubject() {
        subject.click();
    }

    @Step("Выбрать из выпадающего списка значение «Работа ФЦК» = value.")
    public void setSelectSubjectEvents(String value) {
        new Select(subject).selectByValue(value);
    }

    // Selenide
//    @Step("Выбрать из выпадающего списка значение «Работа ФЦК» = value.")
//    public void setSelectSubjectEventS(String value) {
//        subjectS.selectOptionByValue(value);
//    }

    @Step("В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».")
    public void clickSubSubject() {
        subsubject.click();
    }

    // Selenide
//    @Step("В разделе «Основная информация» заполнить поле «Подтема» - нажать на поле «Подтема».")
//    public void clickSubSubjectS() {
//        subsubjectS.hover();
//    }

    @Step("Выбрать из выпадающего списка выбрать значение «Другое» = {value}.")
    public void setSelectSubSubject(String value) {
        new Select(subsubject).selectByValue(value);
    }

    // Selenide
//    @Step("Выбрать из выпадающего списка выбрать значение «Другое» = {value}.")
//    public void setSelectSubSubjectS(String value) {
//        subsubjectS.selectOptionByValue(value);
//    }

    @Step("Клик по дроп-меню - «Email для связи»")
    public void clickEmailForCommunication() {
        new Actions(driver)
                .moveToElement(contact_emails)
                .click();
    }

    @Step("В поле «Email для связи» выбрать Email из предусловия.")
    public void clickToValueEmailForCommunication() {
        contactEmailValueWithTest.click();
    }

    public WebElement getDontAssigned() {
        return dontAssigned;
    }

    public WebElement getConditionInput() {
        return conditionInput;
    }

    @Step("В разделе «Контактная информация» поставить галочку в чек-бокс «Анонимно?»")
    public void clickAnonCheck() {
        anon.click();
    }

    @Step("В поле «Телефон для связи» указать телефон по маске XXXХХХXXXX = {phone}.")
    public void setConnectAnonPhone(String phone) {
        connectAnonPhone.sendKeys(phone);
    }

    public WebElement getConnectAnonPhone() {
        new Actions(driver)
                .moveToElement(connectAnonPhoneForAssert)
                .perform();

        return connectAnonPhoneForAssert;
    }

    @Step("В форме просмотра обращения нажать на кнопку «Назначить ответственного».")
    public void clickAppointAResponsible() {
        appointAResponsible.click();
    }

}
