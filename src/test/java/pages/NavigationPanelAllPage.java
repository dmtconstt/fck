package pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;


public class NavigationPanelAllPage {
    protected WebDriver driver;

    @FindBy(id = "grouptab_1")
    private WebElement allMenu;

    @FindBy(css = ".all [id$='moduleTab_9_Физические лица']")
    private WebElement contacts;

    @FindBy(css = ".all [id$='moduleTab_9_Предприятия']")
    private WebElement аccounts;

    @FindBy(xpath = "(//*[contains(@id,'with-label')])[1]")
    private WebElement accountIconOnTheRight;

    @FindBy(xpath = "(//*[contains(@id,'admin_link')])[3]")
    private WebElement adminButton;

    @FindBy(xpath = "(//*[contains(@id,'admin_link')])[3]")
    private WebElement adminMenu;

    @FindBy(xpath = "(//*[contains(text(),\"Обращения\")])[3]")
    private WebElement appealsMenu;

//    public SelenideElement appealsMenuS = $(byXpath("(//*[contains(text(),\"Обращения\")])[3]"));
//
//    public SelenideElement allMenuS = $("#grouptab_1");

    public NavigationPanelAllPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

//    public NavigationPanelAllPage() {}

    @Step("На основной закладке домашней страницы выбрать закладку модулей «Все».")
    public void allMenu() {
        new Actions(driver)
                .moveToElement(allMenu)
                .click()
                .perform();
    }

    // Selenide
//    @Step("На основной закладке домашней страницы выбрать закладку модулей «Все».")
//    public void allMenuS() {
//        allMenuS.hover();
//    }

    @Step("В выпадающем списке выбрать пункт «Физические лица» («Contacts»).")
    public void clickPhisics() {
        new Actions(driver)
                .moveToElement(contacts)
                .click()
                .perform();
    }

    @Step("Из выпадающего списка выбрать пункт «Предприятия» («Accounts»).")
    public void clickAccounts() {
        new Actions(driver)
                .moveToElement(аccounts)
                .click()
                .perform();
    }

    @Step("Клик по иконке с аккаунтом в правом верхнем углу")
    public void clickToAccountIconOnTheRight() {
        new Actions(driver)
                .moveToElement(accountIconOnTheRight)
                .click()
                .perform();
    }

    @Step("Открыть модуль «Администрирование».")
    public void clickToAdmin() {
        new Actions(driver)
                .moveToElement(adminButton)
                .click()
                .perform();
    }

    @Step("Открыть модуль «Администрирование».")
    public void clickToAdminMenu() {
        new Actions(driver)
                .moveToElement(adminMenu)
                .click()
                .perform();
    }

    @Step("Из выпадающего списка выбрать пункт «Обращения».")
    public void clickToAppealsMenu() {
        new Actions(driver)
                .moveToElement(appealsMenu)
                .click()
                .perform();
    }

    // selenide
//    @Step("Из выпадающего списка выбрать пункт «Обращения».")
//    public void clickToAppealsMenuS() {
//        appealsMenuS.scrollTo().click();
//    }


}
