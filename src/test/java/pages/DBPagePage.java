package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DBPagePage extends MainPagePage {
    private String url = "http://fckproject.itfbgroup.ru/phpmyadmin/index.php";
    private String userDb = "user_auto";
    private String passDb = "crmbdpasswauto";

    // locators
    @FindBy(css = "#input_username")
    private WebElement userNameDb;

    @FindBy(css = "#input_password")
    private WebElement userPasswordDb;

    @FindBy(css = "#input_go")
    private WebElement buttonIntoGo;

    @FindBy(xpath = "//*[contains(text(),\"db_auto\")]")
    private WebElement db_auto;

    @FindBy(css = ".ic_b_sql")
    private WebElement sqlButton;

    @FindBy(css = ".database.selected")
    private WebElement databaseSelected;

    @FindBy(xpath = "(//*[contains(@class, 'CodeMirror-sizer')])[2]")
    private WebElement areaForSqlCommand;

    @FindBy(css = "#button_submit_query")
    private WebElement buttonSubmitQuery;

    @FindBy(css = ".data.text")
    private WebElement responseSqlQuery;

    //metods
    public DBPagePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        PageFactory.initElements(driver, this);
    }

    @Step("авторизация в СУБД")
    public void authorize() {
        this.userNameDb.sendKeys(userDb);
        this.userPasswordDb.sendKeys(passDb);
        new DBPagePage(this.driver, this.wait);
    }

    @Step("Перейти на сайт СУБД из предусловия.")
    public void open() {
        driver.navigate().to(url);
    }

    @Step("Нажать на кнопку \"Вперед\"")
    public void clickButtonIntoGo() {
        buttonIntoGo.click();
    }

    @Step("На боковой панели выбрать БД «db_auto»")
    public void clickDbAuto() {
        db_auto.click();
    }

    @Step("На основной панели нажать на кнопку «SQL».")
    public void clickSqlButton() {
        sqlButton.click();
    }

    @Step("Ожидание выбора БД")
    public void waitSelectDb() {
        wait.until(ExpectedConditions.visibilityOf(databaseSelected));
    }

    @Step(" В поле ввода запроса ввести запрос: {sqlCommand}")
    public void writeSqlCommand(String sqlCommand) {
        new Actions(driver)
                .moveToElement(areaForSqlCommand)
                .sendKeys(sqlCommand)
                .perform();

    }

    @Step("Нажать на кнопку \"Вперед\"")
    public void clickButtonSubmitQuery() {
        buttonSubmitQuery.click();
    }

    @Step("Проверить в окне поиска сотрудников, доступных для назначения,\n" +
            "        // в адресной строке id группы\n" +
            "        // (%ods_securitygroup_id_advanced=3947f96e-9642-954f-18e0-5f74540d8ea9%)")
    public String getResultIdSqlResponse() {
        return responseSqlQuery.getText();
    }

}
