package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ClientSearchWindowPagePage extends MainPagePage {

    // locators
    @FindBy (css = "#last_name_advanced")
    private WebElement lastNameAdvanced;

    @FindBy (css = "#first_name_advanced")
    private WebElement firstNameAdvanced;

    @FindBy (css = "#search_form_submit")
    private WebElement searchFormSubmit;

    // metods
    public ClientSearchWindowPagePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @Step("В поле «Фамилия» ввести фамилию сотрудника = {lastName}, на которого необходимо назначить обращение – фамилия сотрудника из предусловия.")
    public void setLastName(String lastName) {
        lastNameAdvanced.sendKeys(lastName);
    }

    @Step("В поле «Имя» ввести имя сотрудника = {firstName}, на которого необходимо назначить обращение – имя сотрудника из предусловия")
    public void setFirstName(String firstName) {
        firstNameAdvanced.sendKeys(firstName);
    }

    @Step("Нажать на кнопку «Найти».")
    public void clickSButtonSearchForm() {
        searchFormSubmit.click();
    }

}
